//
//  Question.h
//  Synonyms and Antonyms
//
//  Created by Richard Holloway on 29/07/2015.
//  Copyright (c) 2015 Richard Holloway. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Question : NSObject

@property (strong, nonatomic) NSString *questionText;
@property (strong, nonatomic) NSArray *words ;
@property (strong, nonatomic) NSString *correctAnswer;

@end
