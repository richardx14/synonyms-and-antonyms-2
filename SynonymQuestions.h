//
//  SynonymQuestions.h
//  Synonyms and Antonyms
//
//  Created by Richard Holloway on 09/08/2015.
//  Copyright (c) 2015 Richard Holloway. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SynonymQuestions : NSObject

@property (nonatomic, readonly) NSString *word ;
@property (nonatomic, readonly) NSString *synonym ;
@property (nonatomic, readonly) NSArray *otherWords ;


@end
