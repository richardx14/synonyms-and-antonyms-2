//
//  ViewController.m
//  Synonyms and Antonyms
//
//  Created by Richard Holloway on 27/07/2015.
//  Copyright (c) 2015 Richard Holloway. All rights reserved.
//

#import "ViewController.h"
#import "GameModel.h"

@interface ViewController ()

// states

@property (nonatomic) BOOL isNewGameState ;
@property (nonatomic) BOOL wordSelectedState ;
@property (nonatomic) BOOL answerSubmittedState ;
@property (nonatomic) BOOL isNewQuestionState ;

// buttons and labels

@property (strong, nonatomic) IBOutlet UIButton *gameButton ;

@property (weak, nonatomic) IBOutlet UIButton *submitAnswerButton;
@property (weak, nonatomic) IBOutlet UIButton *nextQuestionButton;

@property (weak, nonatomic) IBOutlet UILabel *scoreLabel;
@property (weak, nonatomic) IBOutlet UILabel *questionNoLabel;

@property (weak, nonatomic) IBOutlet UILabel *questionTextLabel;
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *wordTileButtonArray;

@property (strong, nonatomic) IBOutlet UILabel *answerResponseLabel;

// other things

@property (strong, nonatomic) NSArray *wordTiles ;
@property (strong, nonatomic) NSString *selectedAnswer ;
@property (strong, nonatomic) GameModel *game ;
@property (nonatomic) NSUInteger noOfQuestions ;
@property (nonatomic) NSArray *wordSelected ;

@end

@implementation ViewController

- (void)viewDidLoad {

    [super viewDidLoad];
    
    //
    
    self.questionTextLabel.lineBreakMode = NSLineBreakByWordWrapping;
    
    self.questionTextLabel.numberOfLines = 0;
    [self.questionTextLabel sizeToFit];
    
    // states
    
    self.isNewGameState = TRUE ;
    self.isNewQuestionState = TRUE ;
    self.answerSubmittedState = FALSE ;
    self.wordSelectedState = FALSE ;
    
    self.noOfQuestions = 10 ;
    
    self.game = [[GameModel alloc] initNewGameWithMaxNumberOfQuestions: self.noOfQuestions  ] ;
    
    // words on tileButtons - move this to updateUI
    
    [self.game getNextQuestion] ;
    
    // update UI
    
    [self updateUI] ;
    
}

- (IBAction)touchWordButton:(UIButton *)sender {
    
    [self touchButtonLog:sender] ;
    
    if (sender.selected == FALSE) {
        
        for (UIButton *button in self.wordTileButtonArray) {
            
            button.selected = FALSE ;
        }
        
        sender.selected = TRUE ;
        
        self.wordSelectedState = TRUE ;
        
        self.selectedAnswer = sender.titleLabel.text ;
        
    } else {
        
        sender.selected = FALSE ;
        
        self.selectedAnswer = @"" ;
        
        self.wordSelectedState = FALSE ;
        
    }
    
    NSLog(@"ViewController: selectedAnswer = %@", self.selectedAnswer) ;
    
    // states
    
    self.isNewGameState = FALSE ;
    self.isNewQuestionState = FALSE ;
    self.answerSubmittedState = FALSE ;
    
    // update UI
    
    [self updateUI] ;
    
}

- (IBAction)touchSubmitAnswerButton:(UIButton *)sender {
    
    [self touchButtonLog:sender] ;
    
    [self.game checkAnswer:self.selectedAnswer] ;
    
    self.answerResponseLabel.text = [NSString stringWithFormat:@"Correct answer is %@", self.game.question.correctAnswer] ;
    
    // states
    
    self.isNewGameState = FALSE ;
    self.isNewQuestionState = FALSE ;
    self.answerSubmittedState = TRUE ;
    self.wordSelectedState = TRUE ;
    
    // update UI
    
    [self updateUI] ;
    
}

- (IBAction)touchNextQuestionButton:(id)sender {
    
    // states
    
    self.isNewGameState = FALSE ;
    self.isNewQuestionState = TRUE ;
    self.answerSubmittedState = FALSE ;
    self.wordSelectedState = FALSE ;
    
    [self.game getNextQuestion] ;
    
    [self updateUI] ;
}

- (IBAction)touchNewGameButton:(UIButton *)sender {
    
    [self touchButtonLog:sender] ;
    
    self.noOfQuestions = 10 ;
    
    self.game = [[GameModel alloc] initNewGameWithMaxNumberOfQuestions: self.noOfQuestions  ] ;
    
    [self.game getNextQuestion] ;
    
    // states
    
    self.isNewGameState = FALSE ;
    self.isNewQuestionState = TRUE ;
    self.answerSubmittedState = FALSE ;
    self.wordSelectedState = FALSE ;
    
    // update UI
    
    [self updateUI] ;
}

/////////

- (void) updateUI {
    
    // score and question number
    
    self.scoreLabel.text = [NSString stringWithFormat:@"Score: %i", self.game.score] ;
    
    self.questionNoLabel.text = [NSString stringWithFormat:@"Question: %i", self.game.currentQuestionNumber] ;
    
    // new game
    
    if (self.isNewGameState) {

        self.questionTextLabel.text = self.game.questionText ;
        
        [self.wordTileButtonArray[0] setTitle:[self.game.possibleWords objectAtIndex:0] forState:UIControlStateNormal];
        [self.wordTileButtonArray[1] setTitle:[self.game.possibleWords objectAtIndex:1] forState:UIControlStateNormal];
        [self.wordTileButtonArray[2] setTitle:[self.game.possibleWords objectAtIndex:2] forState:UIControlStateNormal];
        [self.wordTileButtonArray[3] setTitle:[self.game.possibleWords objectAtIndex:3] forState:UIControlStateNormal];
        
        self.submitAnswerButton.enabled = FALSE ;
        
        self.nextQuestionButton.hidden = TRUE ;
        
        for (UIButton *button in self.wordTileButtonArray) {
        
            button.enabled = TRUE ;
            button.selected = FALSE ;
        }
        
        self.questionTextLabel.hidden = FALSE ;
        
        self.answerResponseLabel.hidden = TRUE ;
        
        self.isNewGameState = FALSE ;
    }

    // word selected
    
    if (self.wordSelectedState) {
        
        self.submitAnswerButton.enabled = TRUE ; // make this conditional on word button being selected
        
        self.questionTextLabel.hidden = FALSE ;
        
        self.answerResponseLabel.hidden = TRUE ;
        
    } else {
        
        self.submitAnswerButton.enabled = FALSE ;
    }

    // answer submitted
    
    if (self.answerSubmittedState) {
        
        self.answerResponseLabel.hidden = FALSE ;
        self.nextQuestionButton.hidden = FALSE ;
        self.submitAnswerButton.enabled = FALSE ;
        self.answerResponseLabel.text = self.game.answerReponseText ;
        
    } else {
        
        self.answerResponseLabel.hidden = TRUE ;
        self.nextQuestionButton.hidden = TRUE ;
    }
    
    if (self.isNewQuestionState) {
        
        self.submitAnswerButton.enabled = FALSE ;
        
        self.nextQuestionButton.hidden = TRUE ;
        
        self.questionTextLabel.text = self.game.questionText ;
        
        [self.wordTileButtonArray[0] setTitle:[self.game.possibleWords objectAtIndex:0] forState:UIControlStateNormal];
        [self.wordTileButtonArray[1] setTitle:[self.game.possibleWords objectAtIndex:1] forState:UIControlStateNormal];
        [self.wordTileButtonArray[2] setTitle:[self.game.possibleWords objectAtIndex:2] forState:UIControlStateNormal];
        [self.wordTileButtonArray[3] setTitle:[self.game.possibleWords objectAtIndex:3] forState:UIControlStateNormal];

        
        for (UIButton *button in self.wordTileButtonArray) {
            
            button.enabled = TRUE ;
            button.selected = FALSE ;
        }
        
        self.questionTextLabel.hidden = FALSE ;
        
        self.answerResponseLabel.hidden = TRUE ;
        
        self.isNewGameState = FALSE ;

    }
    
}

- (void) touchButtonLog:(UIButton *) button {
    
    NSLog(@"ViewController: Touched button: %@", button.currentTitle) ;
    
}

@end
