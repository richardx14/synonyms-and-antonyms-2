//
//  GameModel.h
//  Synonyms and Antonyms
//
//  Created by Richard Holloway on 27/07/2015.
//  Copyright (c) 2015 Richard Holloway. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Question.h"

@interface GameModel : NSObject

- (Question *) question ; // remove ???

@property (nonatomic, readonly) int score;
@property (nonatomic, readonly) int currentQuestionNumber ;

@property (nonatomic, readonly) NSString *questionText ;
@property (nonatomic, readonly) NSMutableArray *possibleWords ;
@property (nonatomic, readonly) NSString *answerReponseText ;

- (id)initNewGameWithMaxNumberOfQuestions:(NSUInteger) maxNoOfQuestions ;
- (BOOL) checkAnswer:(NSString *) submittedAnswer ;
- (void) getNextQuestion ;


@end
