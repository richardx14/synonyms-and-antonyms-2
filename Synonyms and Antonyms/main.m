//
//  main.m
//  Synonyms and Antonyms
//
//  Created by Richard Holloway on 27/07/2015.
//  Copyright (c) 2015 Richard Holloway. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
