//
//  GameModel.m
//  Synonyms and Antonyms
//
//  Created by Richard Holloway on 27/07/2015.
//  Copyright (c) 2015 Richard Holloway. All rights reserved.
//

#import "GameModel.h"

@interface GameModel ()

@property (nonatomic) Question *question ; // remove

@property (nonatomic) int score;
@property (nonatomic) int currentQuestionNumber;
@property (nonatomic) NSString *questionText ;

@property (nonatomic) NSMutableArray *possibleWords ;
@property (nonatomic) NSString *answerReponseText ;

@property (nonatomic) NSUInteger noOfQuestions ;
@property (nonatomic) NSMutableArray *arrayOfQuestions ;

@end


@implementation GameModel

@synthesize currentQuestionNumber ;

- (NSMutableArray *) possibleWords {
    
    if (!_possibleWords) _possibleWords = [[NSMutableArray alloc] init] ;
    
    return _possibleWords ;
    
}

- (NSMutableArray *) arrayOfQuestions {
    
    if (!_arrayOfQuestions) _arrayOfQuestions = [[NSMutableArray alloc] init] ;
    
    return _arrayOfQuestions ;
    
}

- (Question *) question {
    
    if (!_question) _question = [[Question alloc] init ] ;
    
    return _question ;
}

- (id)initNewGameWithMaxNumberOfQuestions:(NSUInteger) maxNoOfQuestions {
    
    NSLog(@"GameModel initNewGameWithMaxNumberOfQuestions called") ;
    
    self = [super init];
    
    [self populateArrayOfQuestions] ;
    
    self.score = 0 ;
    self.currentQuestionNumber = 0 ;

    // self.questionText = @"Question text from game model property" ;
    
    /* self.possibleWords = [[NSMutableArray alloc] initWithObjects:
                          @"word0 from game model property",
                          @"word1 from game model property",
                          @"word2 from game model property",
                          @"word3 from game model property",
                          nil];
    */
    
    self.noOfQuestions = maxNoOfQuestions ;
    
    return self ;
    
    
}

- (void) getNextQuestion {
    
    NSLog(@"GameModel getNextQuestion called") ;
    
    self.question = [self.arrayOfQuestions objectAtIndex:0] ;
    
    // self.questionText = @"Question text from model property get next question" ;
    
    self.questionText = self.question.questionText ;
    
    NSArray *words = [[NSArray alloc] initWithObjects:
                      @"Word 0 from getNextQuestion",
                      @"Word 1 from getNextQuestion",
                      @"Word 2 from getNextQuestion",
                      @"Word 3 from getNextQuestion",nil] ;
    
    self.possibleWords = [[NSMutableArray alloc ] initWithArray:words] ;
    
    self.possibleWords = [[NSMutableArray alloc ] initWithArray:self.question.words] ;
    
    self.currentQuestionNumber ++ ;
    
}



- (BOOL) checkAnswer:(NSString *) submittedAnswer {
    
    NSLog(@"GameModel checkAnswer called with submitted answer %@", submittedAnswer) ;
    
    if ([submittedAnswer isEqualToString:self.question.correctAnswer])  {
        
        NSLog(@"submittedAnswer is correct") ;

        self.answerReponseText = [NSString stringWithFormat:@"Correct!"] ;

        self.score ++ ;
        
        return TRUE ;
        
    } else {
        
        NSLog(@"submittedAnswer is incorrect") ;

        self.answerReponseText = [NSString stringWithFormat:@"Incorrect. The correct answer is %@",self.question.correctAnswer] ;
        
        return FALSE ;
        
    }
    
    
}

- (void) populateArrayOfQuestions {
    
    [self.arrayOfQuestions removeAllObjects] ;
    
    NSArray *synonyms = [[NSArray alloc] initWithObjects: @"fast",	@"ponder",	@"immense",	@"noise",	@"latter",	@"irony",	@"terrible",	@"cheap",	@"feeble",	@"humidity",	@"disgusting",	@"dense",	@"nervous",	@"sure",	@"amazing",	@"hilarious",	@"shy",	@"fair",	@"conservation",	@"respect",	@"cajole",	@"belief",	@"verify",	@"promote",	@"region",	@"observe",	@"placate",	@"sociable",	@"amazement",	@"odour",	@"affirm",	@"precious",	@"advise",	@"responsible",	@"outcast",	@"boast",	@"passion",	@"establish",	@"outstanding",	@"proceed",	@"swell",	@"modern",	@"frugal",	@"awareness",	@"wander",	@"unique",	@"bountiful",	@"fatigued",	@"submissive",	@"diligent",	@"retard",	@"vagrant",	@"forsake",	@"conceited",	@"incision",	@"fringe",	@"modify",	@"artful",	@"forgery",	@"solemn",	@"absurd",	@"mishmash",	@"humid",	@"ignoramus",	@"placid",	@"leveret",	@"strewn",	@"idle",	@"meek",	@"barren",	@"outlaw",	@"cease",	@"emerge",nil] ;
    
    NSArray *synonymsCorrectAnswers = [[NSArray alloc] initWithObjects:@"rapid",	@"contemplate",	@"large",	@"din",	@"last",	@"sarcasm",	@"appaling",	@"inexpensive",	@"infirm",	@"dampness",	@"gross",	@"crowded",	@"anxious",	@"certain",	@"incredible",	@"very funny",	@"coy",	@"equal",	@"preservation",	@"honour",	@"entice",	@"opinion",	@"confirm",	@"encourage",	@"country",	@"percieve",	@"pacify",	@"gregarious",	@"astonishment",	@"smell",	@"state",	@"valuable",	@"recommend",	@"accountable",	@"exile",	@"brag",	@"enthusiasm",	@"create",	@"remarkable",	@"continue",	@"enlarge",	@"current",	@"thrifty",	@"consciousness",	@"meander",	@"special",	@"abundant",	@"weary",	@"compliant",	@"industrious",	@"detain",	@"tramp",	@"abandon",	@"arrogant",	@"cut",	@"border",	@"alter",	@"wily",	@"counterfeit",	@"serious",	@"ridiculous",	@"mayhem",	@"moist",	@"dunce",	@"sedate",	@"kitten",	@"dispersed",	@"lazy",	@"humble",	@"infertile",	@"banish",	@"terminate",	@"appear", nil] ;
    

    NSMutableArray *possibleAnswers = [[NSMutableArray alloc] initWithArray:synonyms] ;
    
    [possibleAnswers addObjectsFromArray:synonymsCorrectAnswers] ;
    
    NSString *word0, *word1, *word2 ;
    
    for (int i=0; i< [synonyms count]; i++)
    {
        
        // refactor this - a lot of code in this loop ???
        
        Question *question = [[Question alloc] init] ;
        
        question.questionText = [self formQuestionText:[synonyms objectAtIndex:i]];
        
        question.correctAnswer = [synonymsCorrectAnswers objectAtIndex:i] ;
        
        int index = arc4random() % [synonymsCorrectAnswers count] ;
        
        word0 = [synonymsCorrectAnswers objectAtIndex:index] ;
        
        index = arc4random() % [synonymsCorrectAnswers count] ;
        
        word1 = [synonymsCorrectAnswers objectAtIndex:index] ;
        
        index = arc4random() % [synonymsCorrectAnswers count] ;
        
        word2 = [synonymsCorrectAnswers objectAtIndex:index] ;
        
        NSMutableArray *words = [[NSMutableArray alloc] initWithObjects:question.correctAnswer,word0,word1,word2, nil];
        
        NSMutableArray *shuffledWords = [[NSMutableArray alloc] init] ;
        

        // put in loop ???
        // first word
        
        int i = arc4random() % [words count] ;
        
        [shuffledWords addObject:[words objectAtIndex:i]] ;
        
        [words removeObjectAtIndex:i ];
        
        // second word
        
        i = arc4random() % [words count] ;
        
        [shuffledWords addObject:[words objectAtIndex:i]] ;
        
        [words removeObjectAtIndex:i ];

        // third word
        
        i = arc4random() % [words count] ;
        
        [shuffledWords addObject:[words objectAtIndex:i]] ;
        
        [words removeObjectAtIndex:i ];
        
        // fourth word
        
        i = arc4random() % [words count] ;
        
        [shuffledWords addObject:[words objectAtIndex:i]] ;
        
        [words removeObjectAtIndex:i ];
        
        ////////////

        question.words = [[NSArray alloc] initWithArray:shuffledWords] ;
        
        [self.arrayOfQuestions addObject:question] ;
        
        // NSLog(@"Question %i = %@, answer = %@, words = %@", i, question.questionText, question.correctAnswer, question.words.description) ;
    }
    
}

// utilities

- (NSString *) formQuestionText : (NSString *) word {
    
    NSString *questionText = [NSString stringWithFormat:@"Which of the following words has a similar meaning to %@?", word] ;
    
    return questionText ;
}

- (NSArray *) formWordArrayAndShuffle : (NSString *) words{
    
    
    
    return ([[NSArray alloc] init]) ;
    
    
}

@end
