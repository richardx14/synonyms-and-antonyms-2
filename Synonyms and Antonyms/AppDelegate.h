//
//  AppDelegate.h
//  Synonyms and Antonyms
//
//  Created by Richard Holloway on 27/07/2015.
//  Copyright (c) 2015 Richard Holloway. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

