//
//  Question.m
//  Synonyms and Antonyms
//
//  Created by Richard Holloway on 29/07/2015.
//  Copyright (c) 2015 Richard Holloway. All rights reserved.
//

#import "Question.h"

@interface Question ()

// @property (strong, nonatomic) NSString *questionText;
// @property (strong, nonatomic) NSArray *words ;
// @property (strong, nonatomic) NSString *correctAnswer;
//
@end

@implementation Question

- (NSString *) questionText {
    
    if (! _questionText) _questionText = @"What is the meaning of the word match" ;
    
    return _questionText ;
    
}



- (NSArray *) words {

    if (! _words) _words = [[NSArray alloc] initWithObjects:@"word 0 from question", @"word 1 form question", @"word 2 from question", @"word 3 from question", nil] ;
    
    return _words ;

}



- (NSString *) correctAnswer{
    
    if (! _correctAnswer) _correctAnswer = @"word 2 from question" ;
    
    return _correctAnswer ;

}



@end
